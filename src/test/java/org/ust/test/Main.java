package org.ust.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.ust.objectdata.ObjectDataManager;
import org.ust.objectdata.ObjectDataManagerFactory;
import org.ust.testdata.TestDataManager;
import org.ust.testdata.TestDataManagerFactory;
import org.ust.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class Main {

    static TestDataManager testDataManager;
    static ObjectDataManager objectDataManager;
    static String browserName;
    static String devicePlatform;
    static String evidencePath;
    static String dependentFilesPath;

    static WebDriver driver;
    static Properties prop;
    static Util util;

    static Properties runtimeArgs = new Properties();
    static String ARSG_FILE;

    static int port= 0;

    ////Setup method for the TestSuite/TestRun
    @Test
    public void setUp() {
        try {
            devicePlatform = "WINDOWS-CHROME";
            evidencePath=System.getProperty("EVIDENCE_PATH");
            dependentFilesPath = System.getProperty("DEPENDENT_FILES_PATH");
            String platform[] = null;
            if(devicePlatform != null && !devicePlatform.isEmpty())
                platform = devicePlatform.split("-");

            browserName = platform[1];
            ARSG_FILE = "RuntimeArgs.txt";
            loadProperties(ARSG_FILE);

            try{
                String testDataFilePath = "TestData.xlsx";

                if(testDataFilePath != null && !testDataFilePath.isEmpty()){
                    testDataManager = TestDataManagerFactory.getTestDataManager("Excel", testDataFilePath);
                } else {
                    System.out.println("Could not load test data");
                    throw new Exception("Unable to load test data");
                }
            } catch(Exception ex){
                System.out.println("Could not load test data" + ex.getMessage());
                throw ex;
            }

            try{
                String objectDataFilePath = "ObjectData.xlsx";
                if(objectDataFilePath != null && !objectDataFilePath.isEmpty()){
                    objectDataManager = ObjectDataManagerFactory.getObjectDataManager("Excel", objectDataFilePath);
                } else {
                    System.out.println("Could not load object data");
                    throw new Exception("Unable to load object data");
                }
            } catch(Exception ex){
                System.out.println("Could not load object data" + ex.getMessage());
                throw ex;
            }

            switch(browserName) {
                case "FIREFOX":
                    DesiredCapabilities caps = DesiredCapabilities.firefox();
                    LoggingPreferences logPrefs = new LoggingPreferences();
                    logPrefs.enable(LogType.BROWSER, Level.ALL);
                    caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                    driver=new FirefoxDriver(caps);
                    break;
                case "IE":
                    System.setProperty("webdriver.ie.driver","IEDriverServer.exe");
                    System.setProperty("webdriver.ie.driver.loglevel","DEBUG");

                    caps = DesiredCapabilities.internetExplorer();
                    logPrefs = new LoggingPreferences();
                    logPrefs.enable(LogType.BROWSER, Level.ALL);

                    caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                    caps.setCapability("ie.ensureCleanSession",true);
                    driver = new InternetExplorerDriver(caps);
                    break;
                case "CHROME":
                    System.setProperty("webdriver.chrome.driver","chromedriver.exe");
                    System.setProperty("webdriver.chrome.silentOutput", "true");

                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("--disable-dev-shm-usage");
                    options.addArguments("--no-sandbox");
                    options.addArguments("--disable-features=VizDisplayCompositor");

                    if(runtimeArgs != null && runtimeArgs.containsKey("user-data-dir") && runtimeArgs.getProperty("user-data-dir") != null) {
                        options.addArguments("user-data-dir="+runtimeArgs.getProperty("user-data-dir"));
                    }

                    if(runtimeArgs.containsKey("isHeadless") && runtimeArgs.getProperty("isHeadless") != null
                            && !runtimeArgs.getProperty("isHeadless").trim().isEmpty() && runtimeArgs.getProperty("isHeadless").trim().equalsIgnoreCase("YES"))
                        options.addArguments("headless");

                    caps = DesiredCapabilities.chrome();
                    logPrefs = new LoggingPreferences();
                    logPrefs.enable(LogType.BROWSER, Level.ALL);
                    caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                    caps.setCapability("chromeOptions",options);
                    ChromeDriverService chromeDriverService = ChromeDriverService.createDefaultService();
                    port = chromeDriverService.getUrl().getPort();
                    driver = new ChromeDriver(chromeDriverService, caps);

                    System.out.println("Launching Chromedriver on Port : " + port);

                    if(runtimeArgs != null && runtimeArgs.containsKey("environment") && runtimeArgs.getProperty("environment") != null
                            && runtimeArgs.getProperty("environment").trim().equalsIgnoreCase("desktop")) {
                        Thread.sleep(10000);
                        for (String window : driver.getWindowHandles()) {
                            driver.switchTo().window(window);
                        }
                    }
                    break;
                case "SAFARI":
                    caps = DesiredCapabilities.safari();
                    logPrefs = new LoggingPreferences();
                    logPrefs.enable(LogType.BROWSER, Level.ALL);
                    caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                    driver=new SafariDriver(caps);
                    break;
                case "EDGE":
                    System.setProperty("webdriver.edge.driver","MicrosoftWebDriver.exe");
                    org.openqa.selenium.edge.EdgeOptions edgeOptions = new org.openqa.selenium.edge.EdgeOptions();
                    edgeOptions.setPageLoadStrategy("normal");
                    caps = DesiredCapabilities.edge();
                    logPrefs = new LoggingPreferences();
                    logPrefs.enable(LogType.BROWSER, Level.ALL);
                    caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                    caps.setCapability(org.openqa.selenium.edge.EdgeOptions.CAPABILITY, edgeOptions);
                    EdgeDriverService edgeDriverService = EdgeDriverService.createDefaultService();
                    port = edgeDriverService.getUrl().getPort();
                    driver = new org.openqa.selenium.edge.EdgeDriver(edgeDriverService, caps);
                    System.out.println("Launching Edge driver on Port : " + port);
                    break;
                case "FIREFOXGECKO":
                    System.setProperty("webdriver.gecko.driver","geckodriver.exe");
                    driver = new FirefoxDriver();
                    break;
            }
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }  catch (Exception e) {
            System.out.println("initialization failed."+e.getMessage());
            cleanup();
        }
    }

    //Cleanup method for the TestSuite/TestRun
    @AfterSuite
    public void cleanup() {
        try {
            System.out.println("TearDown Selenium");
            if (driver != null)
                driver.quit();
        }  catch (Exception e) {
            System.out.println("Exception occurred while quitting from driver.");
        }
    }

    public static org.openqa.selenium.WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(WebDriver driver){
        Main.driver = driver;
    }

    public static void setDriver(){
        Main.driver = driver;
    }

    public static java.lang.String getDevicePlatform(){
        return devicePlatform;
    }

    public static void setDevicePlatform(){
        Main.devicePlatform = devicePlatform;
    }

    public static java.lang.String getdependentFilesPath(){
        return dependentFilesPath;
    }

    public static void setdependentFilesPath(){
        Main.dependentFilesPath = dependentFilesPath;
    }

    public static org.ust.testdata.TestDataManager getTestDataManager(){
        return testDataManager;
    }

    public static org.ust.objectdata.ObjectDataManager getObjectDataManager(){
        return objectDataManager;
    }

    public static java.lang.String getBrowserName()	{
        return browserName;
    }

    public static String getEvidencePath(){
        return evidencePath;
    }

    public static void setEvidencePath(){
        Main.evidencePath = evidencePath;
    }

    public static java.util.Properties getIdentifiersPropertyFile()	{
        return prop;
    }

    public static void loadProperties(String configFile) {
        File file = new File(configFile);
        try {
            if (file == null || !file.exists())
                return;
            runtimeArgs.load(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
