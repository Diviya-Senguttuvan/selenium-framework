package org.ust.test;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.ust.testdata.TestDataManager;
import org.ust.util.Util;

public class WebTest {

    public TestDataManager testDataManager;
    public WebDriver driver;
    public Actions action;
    public String browserName;
    public WebElement element;


    public String testCaseName;
    public String actualTestCaseName;
    public String testCaseDesc;
    public Util util;

    public static int index=0;
    public static boolean multiKeyStrokeFailed = false;
    
    String value="";
    String id="";
    String Location="";

    @BeforeMethod
    public void setUp() {
        testDataManager = Main.getTestDataManager();
        browserName = Main.getBrowserName();
        driver = Main.getDriver();
        util = new Util();
    }

    @Test
    public void WebEvidenceTestMethod() {
        {
            //Step 1 : go to website https://www.google.com
            try {
                String webSiteURL = (String)util.getTestData("weburl");
                driver.get(webSiteURL);
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        {
           //Step 2 : fill field on element Chrome TextBox
            try {
                element = util.findElement("Google SearchBox");
                if(element != null){
                    String searchText = (String)util.getTestData("SearchText[1]");
                    element.sendKeys(searchText);

                }
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
}
