package org.ust.objectdata;

public interface ObjectDataManager {

	Identifier getIdentifier(String identifierName);

}
