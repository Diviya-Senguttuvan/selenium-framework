package org.ust.objectdata;

public class ObjectDataManagerFactory {
	
	public static final String DATA_SOURCE_EXCEL = "Excel";
	public static final String DATA_SOURCE_JSON = "Json";

	public static ObjectDataManager getObjectDataManager(String dataSource, String fileName) {
		if (dataSource == null || dataSource.isEmpty())
			return null;

		if (dataSource.equalsIgnoreCase(DATA_SOURCE_EXCEL)) {
			return new ExcelObjectDataManager(fileName);
		}
		System.out.println("Test Data Source specified is not supported currently");
		return null;
	}
}
