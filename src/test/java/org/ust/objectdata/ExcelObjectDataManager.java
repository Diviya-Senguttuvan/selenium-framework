package org.ust.objectdata;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.ust.testdata.TestData;
import org.ust.testdata.TestDataManager;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class ExcelObjectDataManager implements ObjectDataManager {

	private static int identifierNameIndex = 0;
	private static int typeIndex = 1;

	private static int CHROME_ID_CLS = 2;
	private static int FF_ID_CLS = 3;
	private static int IE_ID_CLS = 4;
	private static int SAFARI_ID_CLS = 5;
	private static int EDGE_ID_CLS = 6;

	private static int CHROME_XPATH = 7;
	private static int FF_XPATH = 8;
	private static int IE_XPATH = 9;
	private static int SAFARI_XPATH = 10;
	private static int EDGE_XPATH = 11;

	private static List<Identifier> identifierList = new ArrayList<>();
	private static Map<String, String> identifiers = new HashMap<>();



	public ExcelObjectDataManager(String fileName) {
		loadTestData(fileName);
	}

	private void loadTestData(String fileName){
		int numberOfSheets = 0;
		FileInputStream fis = null;
		String dataValidStatus = "";

		try {
			// Get the Excel file
			fis = new FileInputStream(fileName);
			Workbook workbook = null;
			if (fileName.endsWith(".xls")) {
				workbook = new HSSFWorkbook(fis);
			} else if (fileName.endsWith(".xlsx")) {
				workbook = new XSSFWorkbook(fis);
			}

			Sheet sheet = workbook.getSheetAt(numberOfSheets);

			if (sheet == null) {
				System.out.println("Unable to find test data worksheet in the specified file : " + fileName);
				return;
			}

			// Check if the sheet contains rows of testcases
			int rowCount = sheet.getPhysicalNumberOfRows();
			int colCount = 0;
			int rowNum = 0;
			int colNum = 0;
			int maxColumns = 0;
			if (rowCount < 3) {
				System.out.println("No rows present in the Test Data worksheet");
			} else {
				System.out.println("Rows present in the Test Data worksheet : " + rowCount);
				for (rowNum = 2; rowNum < rowCount; rowNum++) {
					Row row = sheet.getRow(rowNum);
					if (row == null) {
						continue;
					}

					ObjectData objData = new ObjectData();
					Identifier identifier = new Identifier();

					try {
						Cell idName = row.getCell(CellReference.convertColStringToIndex("A"));
						if (isCellValid(idName)) {
							identifier.identifierName = idName.getStringCellValue();
						} else {
						//	System.out.println("Test Data Name in row : " + rowNum + " and column : A is invalid");
							continue;
						}

						Cell idType = row.getCell(CellReference.convertColStringToIndex("B"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifier.idType = idType.getStringCellValue();
							} else {
								identifier.idType = "xpath";
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("C"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("chrome:id",idType.getStringCellValue());
								identifiers.put("chrome:classname",idType.getStringCellValue());
							} else {
								identifiers.put("chrome:id","");
								identifiers.put("chrome:classname","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("D"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("firefox:id",idType.getStringCellValue());
								identifiers.put("firefox:classname",idType.getStringCellValue());
							} else {
								identifiers.put("firefox:id","");
								identifiers.put("firefox:classname","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("E"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("ie:id",idType.getStringCellValue());
								identifiers.put("ie:classname",idType.getStringCellValue());
							} else {
								identifiers.put("ie:id","");
								identifiers.put("ie:classname","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("F"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("safari:id",idType.getStringCellValue());
								identifiers.put("safari:classname",idType.getStringCellValue());
							} else {
								identifiers.put("safari:id","");
								identifiers.put("safari:classname","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("G"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("gecko:id",idType.getStringCellValue());
								identifiers.put("gecko:classname",idType.getStringCellValue());
							} else {
								identifiers.put("gecko:id","");
								identifiers.put("gecko:classname","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("H"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("edge:id",idType.getStringCellValue());
								identifiers.put("edge:classname",idType.getStringCellValue());
							} else {
								identifiers.put("edge:id","");
								identifiers.put("edge:classname","");
							}
						}

						//XPATH - Starts
						idType = row.getCell(CellReference.convertColStringToIndex("I"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("chrome:xpath",idType.getStringCellValue());
							} else {
								identifiers.put("chrome:xpath","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("J"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("firefox:xpath",idType.getStringCellValue());
							} else {
								identifiers.put("firefox:xpath","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("K"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("ie:xpath",idType.getStringCellValue());
							} else {
								identifiers.put("ie:xpath","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("L"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("safari:xpath",idType.getStringCellValue());
							} else {
								identifiers.put("safari:xpath","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("M"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("gecko:xpath",idType.getStringCellValue());
							} else {
								identifiers.put("gecko:xpath","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("N"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("edge:xpath",idType.getStringCellValue());
							} else {
								identifiers.put("edge:xpath","");
							}
						}
						//XPATH - Ends

						//CSS - Starts
						idType = row.getCell(CellReference.convertColStringToIndex("O"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("chrome:csslocator",idType.getStringCellValue());
							} else {
								identifiers.put("chrome:csslocator","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("P"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("firefox:csslocator",idType.getStringCellValue());
							} else {
								identifiers.put("firefox:csslocator","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("Q"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("ie:csslocator",idType.getStringCellValue());
							} else {
								identifiers.put("ie:csslocator","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("R"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("safari:csslocator",idType.getStringCellValue());
							} else {
								identifiers.put("safari:csslocator","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("S"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("gecko:csslocator",idType.getStringCellValue());
							} else {
								identifiers.put("gecko:csslocator","");
							}
						}

						idType = row.getCell(CellReference.convertColStringToIndex("T"));
						if (isCellValid(idType)) {
							if (idType.getStringCellValue() != null) {
								identifiers.put("edge:csslocator",idType.getStringCellValue());
							} else {
								identifiers.put("edge:csslocator","");
							}
						}
						identifier.identifiers.putAll(identifiers);
						identifierList.add(identifier);
						//CSS - Ends
					} catch(Exception e) {
						System.out.println("Error while processing row : " + rowNum + " " + e.getMessage());
						continue;
					}
				}
			}
			fis.close();
		} catch (Exception e) {
			System.out.println("Unable to load data for Excel file : " + fileName+ " "+ e);
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				System.out.println("Unable to close the Excel Test Data stream "+e.getMessage());
			}
		}
	}

	private boolean isCellValid(Cell cell) {
		boolean validCell = false;
		if (cell != null) {
			switch (cell.getCellType()) {
				case Cell.CELL_TYPE_BLANK:
					validCell = false;
					break;
				case Cell.CELL_TYPE_STRING:
					if ((cell.getStringCellValue() == null) || (cell.getStringCellValue() != null && !"".equals(cell.getStringCellValue()) && !" ".equals(cell.getStringCellValue()))) {
						validCell = true;
					}
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if (!(Double.isNaN(cell.getNumericCellValue()))) {
						validCell = true;
					}
					break;
			}
		}
		return validCell;
	}

	@Override
	public Identifier getIdentifier(String identifierName) {
		Identifier id = null;
		try{
			for(Identifier iden : identifierList){
				if(iden != null && iden.identifierName.equalsIgnoreCase(identifierName)){
					id = iden;
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to obtain identifier for name : " + identifierName + " due to " + e.getMessage());
		}
		return id;
	}
}
