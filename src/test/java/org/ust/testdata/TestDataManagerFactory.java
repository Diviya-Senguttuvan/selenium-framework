package org.ust.testdata;

public class TestDataManagerFactory {
	
	public static final String DATA_SOURCE_EXCEL = "Excel";
	public static final String DATA_SOURCE_JSON = "Json";

	public static TestDataManager getTestDataManager(String dataSource, String fileName) {
		if (dataSource == null || dataSource.isEmpty())
			return null;

		if (dataSource.equalsIgnoreCase(DATA_SOURCE_EXCEL)) {
			return new ExcelTestDataManager(fileName);
		} else if (dataSource.equalsIgnoreCase(DATA_SOURCE_JSON)) {	
			return new JsonTestDataManager(fileName);
		}  
		System.out.println("Test Data Source specified is not supported currently");
		return null;
	}
}
