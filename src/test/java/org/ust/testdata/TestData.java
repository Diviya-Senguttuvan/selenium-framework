package org.ust.testdata;

import java.util.*;

public class TestData {
    public String testDataName;
    public String dataType;

    public List<Object> testDataValues = new ArrayList<>();

    public String getTestDataName() {
        return testDataName;
    }

    public void setTestDataName(String testDataName) {
        this.testDataName = testDataName;
    }

    public List<Object> getTestDataValues() {
        return testDataValues;
    }

    public void setTestDataValues(List<Object> testDataValues) {
        this.testDataValues = testDataValues;
    }
}
