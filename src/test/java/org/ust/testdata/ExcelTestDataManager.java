package org.ust.testdata;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.reporters.XMLConstants;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ExcelTestDataManager implements TestDataManager {

	private static int dataNameIndex = 0;
	private static int typeIndex = 1;

	private static Map<String, TestData> testDataList = new HashMap<String,TestData>();

	public ExcelTestDataManager(String fileName) {
		loadTestData(fileName);
	}

	private void loadTestData(String fileName){
		int numberOfSheets = 0;
		FileInputStream fis = null;
		String dataValidStatus = "";

		try {
			// Get the Excel file
			fis = new FileInputStream(fileName);
			Workbook workbook = null;
			if (fileName.endsWith(".xls")) {
				workbook = new HSSFWorkbook(fis);
			} else if (fileName.endsWith(".xlsx")) {
				workbook = new XSSFWorkbook(fis);
			}

			Sheet sheet = workbook.getSheetAt(numberOfSheets);

			if (sheet == null) {
				System.out.println("Unable to find test data worksheet in the specified file : " + fileName);
				return;
			}

			// Check if the sheet contains rows of testcases
			int rowCount = sheet.getPhysicalNumberOfRows();
			int colCount = 0;
			int rowNum = 0;
			int colNum = 0;
			int maxColumns = 0;
			if (rowCount < 2) {
				System.out.println("No rows present in the Test Data worksheet");
			} else {
				System.out.println("Rows present in the Test Data worksheet : " + rowCount);
				for (rowNum = 0; rowNum < rowCount; rowNum++) {
					Row row = sheet.getRow(rowNum);
					if (row == null) {
						continue;
					}

					if (row.getRowNum() < 1) {
						maxColumns = row.getLastCellNum();
						continue;
					} else {
						try {
							// Read the test data info
							TestData testDataItem = null;

							boolean testDataIsMissing = false;

							Cell cell1 = row.getCell(dataNameIndex);
							if (isCellValid(cell1)) {
								testDataItem = new TestData();
								testDataItem.testDataName = cell1.getStringCellValue();
							} else {
								testDataIsMissing = true;
								//System.out.println("Test Data Name in row : " + rowNum + " and column : " + dataNameIndex + " is invalid");
								continue;
							}

							// Mandatory info is present. Get the rest of the details
							Cell cell2 = row.getCell(typeIndex);
							if (isCellValid(cell2)) {
								if (cell2.getStringCellValue() != null) {
									testDataItem.dataType = cell2.getStringCellValue();
								} else {
									testDataItem.dataType = "Text";
								}
							}

							// Fetch max columns for a row based on last length
							for(int colIndex = 2; colIndex < maxColumns; colIndex++) {
								Cell cell = row.getCell(colIndex);
								if (isCellValid(cell)) {
									if (testDataItem.dataType.equalsIgnoreCase("Text")) {
										DataFormatter formatter = new DataFormatter();
										String cell2Value = formatter.formatCellValue(cell);
										testDataItem.getTestDataValues().add(cell2Value);
									} else if (testDataItem.dataType.equalsIgnoreCase("Number")) {
										String value = null;
										try {
											Double valueDouble = cell.getNumericCellValue();
											if (valueDouble != null) {
												value = valueDouble.toString();
											}
										} catch (Exception v) {
											value = cell.getStringCellValue();
										}
										if (value == null) {
											value = "";
										}
										testDataItem.getTestDataValues().add(value);
									} else if (testDataItem.dataType.equalsIgnoreCase("Date")) {
										String value = null;
										try {
											Date valueDate = cell.getDateCellValue();
											if (valueDate != null) {
												value = valueDate.toString();
											}
										} catch (Exception v) {
											value = cell.getStringCellValue();
										}
										if (value == null) {
											value = "";
										}
										testDataItem.getTestDataValues().add(value);
									}
								} else {
								//	System.out.println("Data Value in row : " + rowNum + " and column : " + colIndex + " is invalid");
								}
							}
							if(testDataItem != null) {
								if (testDataList.containsKey(testDataItem.testDataName))
									System.out.println("The Test Data Item is a duplicate of an already existing item. Ignoring it");
								else
									testDataList.put(testDataItem.testDataName, testDataItem);
							}
						} catch(Exception e) {
							System.out.println("Error while processing row : " + rowNum + " " + e.getMessage());
							continue;
						}
					}
				}
				System.out.println("Loaded Test data file : " + fileName);
				System.out.println("Total number of data elements in file : " + testDataList.size());
			}
			fis.close();
		} catch (Exception e) {
			System.out.println("Unable to load data for Excel file : " + fileName+ " "+ e);
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				System.out.println("Unable to close the Excel Test Data stream "+e.getMessage());
			}
		}
	}

	private boolean isCellValid(Cell cell) {
		boolean validCell = false;
		if (cell != null) {
			switch (cell.getCellType()) {
				case Cell.CELL_TYPE_BLANK:
					validCell = false;
					break;
				case Cell.CELL_TYPE_STRING:
					if ((cell.getStringCellValue() == null) || (cell.getStringCellValue() != null && !"".equals(cell.getStringCellValue()) && !" ".equals(cell.getStringCellValue()))) {
						validCell = true;
					}
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if (!(Double.isNaN(cell.getNumericCellValue()))) {
						validCell = true;
					}
					break;
			}
		}
		return validCell;
	}

	@Override
	public Object getTestData(String dataName, int counter) throws Exception {
		Object returnTestData = null;
		if(testDataList == null || testDataList.isEmpty())
			return returnTestData;

		for(Map.Entry<String, TestData> testDataItem : testDataList.entrySet()) {
			String testDataName = testDataItem.getKey();
			TestData testData = testDataItem.getValue();
			if(testDataName.equalsIgnoreCase(dataName)){
				if(testData != null && testData.dataType.equalsIgnoreCase("Text")){
					try {
						returnTestData = testData.getTestDataValues().get(counter);
					} catch(Exception e) {
						return returnTestData;
					}
				} else if(testData != null && testData.dataType.equalsIgnoreCase("Number")){
					try {
						returnTestData = Integer.parseInt((String) testData.getTestDataValues().get(counter));
					} catch(Exception e) {
						return returnTestData;
					}
				} else if(testData != null && testData.dataType.equalsIgnoreCase("Double")){
					try {
						returnTestData = Double.valueOf((String) testData.getTestDataValues().get(counter));
					} catch(Exception e) {
						return returnTestData;
					}
				} else if(testData != null && testData.dataType.equalsIgnoreCase("Date")){
					try {
						returnTestData = (String) testData.getTestDataValues().get(counter);
					} catch(Exception e) {
						return returnTestData;
					}
				}
			}
		}
		return returnTestData;
	}
}
