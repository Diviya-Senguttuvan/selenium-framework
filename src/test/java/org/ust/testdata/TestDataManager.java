package org.ust.testdata;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface TestDataManager {

	Object getTestData(String dataName, int counter) throws Exception;
}
