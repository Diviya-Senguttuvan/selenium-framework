package org.ust.util;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.ust.objectdata.Identifier;
import org.ust.objectdata.ObjectDataManager;
import org.ust.test.Main;
import org.ust.testdata.TestDataManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Util {

    public TestDataManager testDataManager;
	public ObjectDataManager objectDataManager;
    public WebDriver driver;
    public String browserName;
    
    public Util() {
    	testDataManager = Main.getTestDataManager();
		objectDataManager = Main.getObjectDataManager();
        browserName = Main.getBrowserName().toUpperCase();
        driver = Main.getDriver();
    }

	public Object getTestData(String data) throws Exception {
		int index = 0;
		Object retrieveddata="";

		if(data != null && !data.isEmpty()) {
			if(testDataManager == null) {
				throw new Exception("Invalid test data manager. It is null");
			}

			if(data.indexOf("[") != -1 && data.indexOf("]") !=-1) {
				index = Integer.parseInt(data.substring(data.indexOf("[") + 1, data.lastIndexOf("]")));
				data = data.substring(0,data.lastIndexOf("["));
			}

			retrieveddata = testDataManager.getTestData(data,index);
			if(retrieveddata == null) {
				throw new Exception("Could not retrieve data for key: "+data);
			}
		}
		return retrieveddata;
	}

	public Identifier getIdentity(String idName) {
    	Identifier id = null;
		try	{
			id = objectDataManager.getIdentifier(idName);
		} catch(Exception ex) {
			return null;
		}
		return id;
	}

	public WebElement findElement(String idName) {
		WebElement element = null;
		Identifier id = getIdentity(idName);
		if(id != null) {
			String typeValue = null;
			String idType = id.idType.toLowerCase();
			if(id.identifiers != null && !id.identifiers.isEmpty()) {
				for(Map.Entry<String, String> idEntry : id.identifiers.entrySet()){
					String key = idEntry.getKey();
					String value = idEntry.getValue();
					if(key.equalsIgnoreCase(browserName.toLowerCase()+":"+idType)){
						typeValue = value;
						break;
					}
				}
			}

			switch (idType) {
				case "xpath":
					element = driver.findElement(By.xpath(typeValue));
					break;
				case "csslocator":
					element = driver.findElement(By.cssSelector(typeValue));
					break;
				case "id":
					element = driver.findElement(By.id(typeValue));
					break;
				case "className":
					element = driver.findElement(By.className(typeValue));
					break;
				case "linkText":
					element = driver.findElement(By.linkText(typeValue));
					break;
				case "partialLinkText":
					element = driver.findElement(By.partialLinkText(typeValue));
					break;
				case "name":
					element = driver.findElement(By.name(typeValue));
					break;
				case "tagName":
					element = driver.findElement(By.tagName(typeValue));
					break;
			}
		}
		return element;
	}

    public WebElement findElement(String idName, String idType) {
		WebElement element = null;
    	Identifier id = getIdentity(idName);
    	if(id != null) {
    		String typeValue = null;
			if(id.identifiers != null && !id.identifiers.isEmpty()) {
				for(Map.Entry<String, String> idEntry : id.identifiers.entrySet()){
					String key = idEntry.getKey();
					String value = idEntry.getValue();
					if(key.equalsIgnoreCase(browserName.toLowerCase()+":"+idType.toLowerCase())){
						typeValue = value;
						break;
					}
				}
			}

			switch (idType.toLowerCase()) {
				case "xpath":
					element = driver.findElement(By.xpath(typeValue));
					break;
				case "csslocator":
					element = driver.findElement(By.cssSelector(typeValue));
					break;
				case "id":
					element = driver.findElement(By.id(typeValue));
					break;
				case "className":
					element = driver.findElement(By.className(typeValue));
					break;
				case "linkText":
					element = driver.findElement(By.linkText(typeValue));
					break;
				case "partialLinkText":
					element = driver.findElement(By.partialLinkText(typeValue));
					break;
				case "name":
					element = driver.findElement(By.name(typeValue));
					break;
				case "tagName":
					element = driver.findElement(By.tagName(typeValue));
					break;
			}
		}
        return element;
    }

    // For List operations
    public List<WebElement> findElements(String idName, String idType) {
		List<WebElement> elements = null;
		Identifier id = getIdentity(idName);
		if (id != null) {
			String typeValue = null;
			if (id.identifiers != null && !id.identifiers.isEmpty()) {
				for (Map.Entry<String, String> idEntry : id.identifiers.entrySet()) {
					String key = idEntry.getKey();
					String value = idEntry.getValue();
					if (key.equalsIgnoreCase(browserName.toLowerCase() + ":" + idType.toLowerCase())) {
						typeValue = value;
						break;
					}
				}
			}
			switch (idType.toLowerCase()) {
				case "xpath":
					elements = driver.findElements(By.xpath(typeValue));
					break;
				case "csslocator":
					elements = driver.findElements(By.cssSelector(typeValue));
					break;
				case "id":
					elements = driver.findElements(By.id(typeValue));
					break;
				case "className":
					elements = driver.findElements(By.className(typeValue));
					break;
				case "linkText":
					elements = driver.findElements(By.linkText(typeValue));
					break;
				case "partialLinkText":
					elements = driver.findElements(By.partialLinkText(typeValue));
					break;
				case "name":
					elements = driver.findElements(By.name(typeValue));
					break;
				case "tagName":
					elements = driver.findElements(By.tagName(typeValue));
					break;
			}
		}
		return elements;
	}
}
